const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const app = express();
const port = 4000;
const Papa = require("papaparse");

const compareArrayOfObjects = require("./libs/compArray");
const {
  filterArrayOfObjects,
  filterArrayUnique,
} = require("./libs/filterArray");

const papaConf = {
  header: true,
  newline: "\r\n",
  skipEmptyLines: true,
  worker: true,
};

app.use(cors());
app.use(bodyParser.text({ type: "application/json" }));
app.use(bodyParser.json());

app.get("/", (_req, res) => {
  res.send("<center><h3>nothing to see here :)</h3><center>");
});

app.post("/parser", (request, response) => {
  const dataParsed = JSON.parse(request.body);

  const csv = Papa.parse(dataParsed.csvData, papaConf);

  // TODO: These 'keys' needs to be dynamic
  const csvSorted = csv.data.sort(compareArrayOfObjects("Vat number"));

  const values = Object.values(csvSorted);
  const fields = Object.keys(values[0]);
  const errors = csv.errors;

  const data = {
    fields,
    values,
    errors,
  };

  // Simulate latency
  setTimeout(() => {
    response.end(JSON.stringify(data));
  }, 700);
});

app.post("/calc", (request, response) => {
  const dataCalc = JSON.parse(request.body);

  const inCurrency = dataCalc.inCurrency;
  const outCurrency = dataCalc.outCurrency;
  const csvData = dataCalc.csvData;

  // TODO: These 'keys' needs to be dynamic
  const customers = filterArrayOfObjects(csvData, ["Vat number"]);
  const uniqueCust = filterArrayUnique(customers, "Vat number");
  const separateCustomers = uniqueCust.map((cust) =>
    csvData.filter((row) => row["Vat number"] === cust && row)
  );

  const totalSums = separateCustomers.map((customer) => {
    // TODO: These 'keys' needs to be dynamic
    const currencies = filterArrayOfObjects(customer, ["Currency", "Total"]);
    const convertToEUR = currencies.map((curr) => {
      const converted = inCurrency.map((val) => {
        if (val.code === curr["Currency"]) {
          return curr["Total"] * val.rate;
        }
      });
      return converted.filter(Boolean);
    });
    const sumInEUR = convertToEUR.reduce(
      (accumulator, currentValue) =>
        Math.abs(accumulator) + Math.abs(currentValue)
    );
    const outRate = inCurrency.find((el) => el.code === outCurrency && el.rate);
    const outputSum =
      Math.round((sumInEUR * outRate.rate + Number.EPSILON) * 100) / 100;
    return `${customer[0]["Customer"]} - ${outputSum} ${outCurrency}`;
  });

  // Simulate latency
  setTimeout(() => {
    response.end(JSON.stringify(totalSums));
  }, 700);
});

app.listen(port, () =>
  console.log(`\nListening at http://localhost:${port}\n`)
);
