const { pick } = require("filter-anything");

const filterArrayOfObjects = (array, fields) =>
  array.map((key) => pick(key, fields));

const filterArrayUnique = (array, field) => {
  const uniqueValues = array.map((key) => key[field]);

  return [...new Set(uniqueValues)];
};

module.exports = { filterArrayOfObjects, filterArrayUnique };
