export default [
  {
    code: "EUR",
    rate: "1.000",
  },
  {
    code: "USD",
    rate: "0.987",
  },
  {
    code: "GBP",
    rate: "0.878",
  },
];
