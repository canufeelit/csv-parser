import React, { useState } from "react";
import styled from "@emotion/styled";
import { makeStyles, createStyles } from "@material-ui/core/styles";
import {
  CssBaseline,
  Container,
  Grid,
  Button,
  InputLabel,
  MenuItem,
  FormControl,
  Select,
  TextField,
  Chip,
} from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";

import currencies from "./config/currencies";
import SimpleTable from "./components/Table";
import SumsList from "./components/SumsList";

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      padding: theme.spacing(2),
      "& > * + *": {
        marginTop: theme.spacing(3),
      },
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: "center",
      color: theme.palette.text.secondary,
    },
    input: {
      display: "none",
    },
    button: {
      margin: theme.spacing(1),
      flexGrow: 1,
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 220,
    },
    formButtons: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
    },
    menuItem: {
      justifyContent: "space-between",
    },
    selectInput: {
      justifyContent: "space-between",
      display: "flex",
    },
  })
);

const FormControls = styled("div")`
  display: flex;
  flex-direction: column;
  min-width: 256px;
`;

const resetTable = {
  fields: [],
  values: [],
  errors: [],
};

export default function App() {
  const classes = useStyles();
  const [inCurrency, setInCurrency] = useState(currencies);
  const [outCurrency, setOutCurrency] = useState("EUR");
  const [csvData, setCSVData] = useState([]);
  const [totalSums, setTotalSums] = useState([]);
  const [tableData, setTableData] = useState(resetTable);
  const [parserLoading, setParserLoading] = useState(false);
  const [calcLoading, setCalcLoading] = useState(false);

  const handleFileInputChange = (event) => {
    const fileReader = new FileReader();
    const file = event.target.files[0];

    fileReader.onload = (evt) => {
      setCSVData(evt.target.result);
    };
    if (file) {
      fileReader.readAsText(file, "utf8");
    }
  };

  const handleOutCurrencyChange = (event) => {
    setOutCurrency(event.target.value);
  };

  const handleParse = async () => {
    setParserLoading(true);
    setTableData(resetTable);

    const submitResult = await fetch("http://localhost:4000/parser", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        csvData,
      }),
    }).then((response) => {
      if (response.statusText === "OK") {
        setParserLoading(false);
        return response.text();
      }
    });

    setTableData(JSON.parse(submitResult));
  };

  const handleCalc = async () => {
    setCalcLoading(true);
    setTotalSums([]);
    const totalSums = await fetch("http://localhost:4000/calc", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        inCurrency,
        outCurrency,
        csvData: tableData.values,
      }),
    }).then((response) => {
      if (response.statusText === "OK") {
        setCalcLoading(false);
        return response.text();
      }
    });

    setTotalSums(JSON.parse(totalSums));
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <Container fixed maxWidth="md">
        <div className={classes.root}>
          <Grid container spacing={0} justify="center" alignItems="center">
            <FormControls>
              <FormControl className={classes.formControl}>
                <input
                  accept=".csv"
                  className={classes.input}
                  id="contained-button-file"
                  multiple
                  type="file"
                  onChange={handleFileInputChange}
                />
                <label htmlFor="contained-button-file">
                  <Button
                    variant="contained"
                    color="primary"
                    fullWidth
                    component="span"
                  >
                    Upload
                  </Button>
                </label>
              </FormControl>

              {/**
               * TODO:
               * Rates are currently hardcoded.
               * Should be dynamic.
               */}
              <FormControl className={classes.formControl}>
                <Autocomplete
                  multiple
                  id="tags-standard"
                  options={currencies}
                  getOptionLabel={(option) => option.code}
                  defaultValue={currencies}
                  blurOnSelect
                  autoHighlight
                  fullWidth
                  filterSelectedOptions
                  renderOption={(option) => `${option.code} (${option.rate})`}
                  renderTags={(tagValue, getTagProps) =>
                    tagValue.map((option, index) => (
                      <Chip
                        label={`${option.code} (${option.rate})`}
                        {...getTagProps({ index })}
                      />
                    ))
                  }
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant="standard"
                      label="Input Currency"
                      placeholder="Currency"
                    />
                  )}
                  onChange={(event, value, reason) => {
                    setInCurrency(value);
                  }}
                />
              </FormControl>

              <FormControl className={classes.formControl}>
                <InputLabel id="out-select-label">Out Currency</InputLabel>
                <Select
                  labelId="out-select-label"
                  value={outCurrency}
                  onChange={handleOutCurrencyChange}
                  disabled={!csvData.length && true}
                >
                  {currencies.map(({ code }, i) => (
                    <MenuItem value={code} key={i}>
                      {code}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>

              <FormControl className={classes.formButtons}>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={handleParse}
                  className={classes.button}
                  disabled={!csvData.length && true}
                >
                  Parse
                </Button>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={handleCalc}
                  className={classes.button}
                  disabled={!csvData.length && true}
                >
                  Calculate
                </Button>
              </FormControl>
            </FormControls>
          </Grid>
        </div>
        <div className={classes.root}>
          <Grid container spacing={0} justify="center" alignItems="center">
            <SimpleTable loading={parserLoading} tableData={tableData} />
          </Grid>
        </div>
        <div className={classes.root}>
          <Grid container spacing={0} justify="center" alignItems="center">
            <SumsList loading={calcLoading} totalSums={totalSums} />
          </Grid>
        </div>
      </Container>
    </React.Fragment>
  );
}
