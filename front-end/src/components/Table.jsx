import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  CircularProgress,
} from "@material-ui/core";
import Alert from "@material-ui/lab/Alert";

import invoiceTypes from "../config/invoiceTypes";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

export default function SimpleTable({ tableData, loading }) {
  const classes = useStyles();
  const { fields, values, errors } = tableData;

  if (loading) {
    return <CircularProgress disableShrink />;
  }

  if (!Object.keys(values).length) {
    return null;
  }

  if (errors.length !== 0) {
    return errors.map((error) => (
      <Alert severity="error">{`Error on row ${error.row + 2}: ${
        error.message
      }`}</Alert>
    ));
  }

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            {fields.map((cell, i) => (
              <TableCell align="center" key={i}>
                {cell}
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {values.map((row, i) => {
            const cellValues = Object.values(row);

            const cell = cellValues.map((cell, i) => (
              <TableCell align="center" key={i}>
                {fields[i] === "Type" ? invoiceTypes[cell - 1] : cell}
              </TableCell>
            ));

            return <TableRow key={i}>{cell}</TableRow>;
          })}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
