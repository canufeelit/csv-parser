import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  List,
  ListItem,
  ListItemText,
  CircularProgress,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
}));

const SimpleList = ({ totalSums, loading }) => {
  const classes = useStyles();

  if (loading) {
    return <CircularProgress disableShrink />;
  }

  if (!totalSums.length) {
    return null;
  }

  return (
    <div className={classes.root}>
      <List component="nav">
        {totalSums.map((element, i) => (
          <ListItem button key={i}>
            <ListItemText inset primary={element} />
          </ListItem>
        ))}
      </List>
    </div>
  );
};

export default SimpleList;
