## Setup the Server

Navigate into `express-parser`

### `yarn install`

### `yarn start`

The server will be accessible at [http://localhost:4000](http://localhost:4000)

### `/parser`

To parse the csv file

### `/calc`

To sum the invoices

## Setup the Frontend

Navigate into `front-end`

### `yarn install`

### `yarn start`

The app runs at [http://localhost:3000](http://localhost:3000) to view it in the browser.
